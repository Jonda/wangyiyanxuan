import Vue from 'vue'
import App from './App.vue'
//引入路由器
import router from '@/router'


Vue.config.productionTip = false

new Vue({
  render: h => h(App),
  //传入路由器
  router
}).$mount('#app')
